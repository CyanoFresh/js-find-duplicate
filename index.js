// Find duplicate.js
// By Alex Solomaha <cyanofresh@gmail.com>

function find(arr) {
    for (let i = 0; i < arr.length; i++) {
        if (arr[i] === arr[i + 1]) {
            return arr[i];
        }
    }
}

// Test:
// console.log(find([
//     1, 2, 3, 4, 5, 6, 7, 8, 8, 9, 10
// ]));